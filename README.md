# HouseTest

This has some a simple data model for CRUD of entities via API, UI and gem.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'house_test'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install house_test

## Usage

It has a server and client. To start the server, simply run `house_test`.
The client is defined in the `House` class.

To create a new house
```ruby
House.new(name: 'house name')
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/samuelgarratt/house_test. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the HouseTest project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/samuelgarratt/house_test/blob/master/CODE_OF_CONDUCT.md).
