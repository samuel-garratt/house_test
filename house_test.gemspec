# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'house_test/version'

Gem::Specification.new do |spec|
  spec.name          = 'house_test'
  spec.version       = HouseTest::VERSION
  spec.authors       = ['Samuel Garratt']
  spec.email         = ['samuel.garratt@integrationqa.com']

  spec.summary       = 'Test system for demonstrating CRUD in API and UI testing.'
  spec.description   = 'Test system for demonstrating CRUD in API and UI testing.'
  spec.homepage      = 'https://gitlab.com/samuel-garratt/house_test'
  spec.license       = 'MIT'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '>= 2'
  spec.add_development_dependency 'rack'
  spec.add_development_dependency 'rack-test'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rest-client'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-faker'
  spec.add_dependency 'faker'
  spec.add_dependency 'hashie'
  spec.add_dependency 'sinatra', '2.0.4'
  spec.add_dependency 'sinatra-basic-auth'
  spec.add_dependency 'sinatra-docdsl'
  spec.add_dependency 'soaspec'
end
