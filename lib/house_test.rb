# frozen_string_literal: true

require 'faker'
require 'house_test/version'
require 'house_test/indifferent_hash'
require 'active_support'
ActiveSupport::Deprecation.silenced = true
require 'soaspec'

# The code loaded when simply requiring the gem will only load code for the client
# Require 'house_test/house_server' to host server

module HouseTest
  # Port for server to run at
  PORT = 14_000
end

# House object that talks to the backend and retrieves house results
class HouseHandler < Soaspec::RestHandler
  base_url "http://localhost:#{HouseTest::PORT}/houses"
end

# Object to represent a client creating objects against a server
class House
  # Handles API calls
  attr_accessor :handler
  # Attributes mapping to fields in the backend
  attr_accessor :id

  # Create a new house
  # @param [Hash] params Parameters to create new house with
  #                      name, rooms, bathrooms, garages
  def initialize(params = {})
    self.handler = HouseHandler
    exchange = handler.post(body: params)
    self.id = exchange.response.body
  end

  %i[name rooms bathrooms garages].each do |attribute|
    # Retrieve attribute from backend
    define_method(attribute) do
      HouseHandler.get(id)[attribute.to_s]
    end

    # Set attribute in backend
    define_method("#{attribute}=") do |value|
      HouseHandler.patch(suburl: id, body: { attribute => value }).response
    end
  end
end
