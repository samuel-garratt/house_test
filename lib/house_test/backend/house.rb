# frozen_string_literal: true

module Backend
  # House in memory
  class House
    # Attributes of a house
    attr_accessor :name, :rooms, :bathrooms, :garages, :id
    # Create a new house
    def initialize(house_params)
      params = IndifferentHash.new(house_params)

      self.name = params[:name] || Faker::Name.name
      self.rooms = (params[:rooms] || Faker::Number.number(digits: 1)).to_i
      self.garages = params[:garages] || 1
      self.bathrooms = params[:bathrooms] || rooms - 1
      self.id = SecureRandom.uuid
    end

    # @string [String] JSON representation of House
    def description
      JSON.generate(id: id, name: name, rooms: rooms, bathrooms: bathrooms, garages: garages)
    end
  end
end
