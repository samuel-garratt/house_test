# frozen_string_literal: true

require 'house_test/backend/house'
require 'house_test/server/house_server'
# Representing House objects in 'backend' (memory)
module HouseTest
  # List of houses in memory
  @houses = []

  class << self
    attr_accessor :houses

    # Add a new house according to parameters passed
    # @return [String] Id of house just created
    def add(house_params = {})
      house = Backend::House.new(house_params)
      @houses << house
      house.id
    end

    # Get an existing house
    # @param [Integer] id Id of house to get
    # @return [House] House found. Nil if nothing found
    def get(id)
      houses.find { |house| house.id == id }
    end

    # Update house at id's key to value
    # @param [Integer] id Id of house to update
    # @return [Integer] Id of house updated
    def update(id, key, value)
      found_house = get id
      return nil unless found_house

      found_house.send("#{key}=", value)
      id
    end

    # Delete house at id
    # @param [Integer] id Id of house to delete
    # @return [Integer] Id of house deleted. Nil if not found
    def delete(id)
      found_house = get id
      return nil unless found_house

      houses.delete(found_house)
      id
    end

    # @return [String] Description of all houses
    def descriptions
      houses.map(&:description)
    end
  end
end
