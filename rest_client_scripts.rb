# frozen_string_literal: true

# Example of using RestClient to create/retrieve houses
# Can open with 'restclient' on cmd line
require 'rest_client'
require 'json'

id = RestClient.post('http://localhost:14000/houses', JSON.generate(name: 'test_house')).body
puts id

RestClient.get('localhost:14000/houses').body
