# frozen_string_literal: true

require 'house_test/house_server'

RSpec.describe HouseTest do
  it 'can accumulate houses' do
    previous_count = HouseTest.houses.count
    HouseTest.add
    expect(HouseTest.houses.count).to eq(previous_count + 1)
  end
  it 'can retrieve a specific house' do
    id = HouseTest.add
    retrieved_house = HouseTest.get(id)
    expect(retrieved_house.id).to eq id
  end
  it 'can see description of houses' do
    HouseTest.add
    expect(HouseTest.descriptions).to be_instance_of Array
  end
  it 'can update a house' do
    id = HouseTest.add name: 'Starting name'
    HouseTest.update(id, 'name', 'Second name')
    expect(HouseTest.get(id).name).to eq 'Second name'
  end
  it 'delete house' do
    id = HouseTest.add
    HouseTest.delete id
    expect(HouseTest.get(id)).to be nil
  end
end

RSpec.describe Backend::House do
  it 'has a meaningful description' do
    house = Backend::House.new(name: 'desc')

    expect(JSON.parse(house.description)['name']).to eq 'desc'
  end
  it 'create specific house' do
    house = Backend::House.new(name: 'desc', rooms: '4')
    expect(house.rooms).to eq 4
  end
  it 'can create with strings as keys' do
    house = Backend::House.new('name' => 'string key')
    expect(house.name).to eq 'string key'
  end
end
