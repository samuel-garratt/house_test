# frozen_string_literal: true

RSpec.describe 'House handler' do
  before(:all) do
    @pid = Process.spawn('ruby', 'exe/house_test', err: %w[logs/test_server.log w])
    sleep 2
  end
  describe 'Interact with API' do
    it 'create and retrieve house' do
      create = HouseHandler.post(body: { name: 'test_name' })
      expect(create.status_code).to eq 200
      id = create.response.body
      house = HouseHandler.get(id)
      expect(house.response.body).to include id
    end
    it 'update house' do
      create = HouseHandler.post(body: { name: 'first name' })
      expect(create.status_code).to eq 200
      id = create.response.body
      house = HouseHandler.patch(suburl: id, body: { name: 'second name' })
      expect(house.response.body).to include id
      updated_house = HouseHandler.get(id)
      expect(updated_house.response.body).to include 'second name'
    end
  end
  after(:all) do
    Process.kill(:QUIT, @pid)
  end
end
