# frozen_string_literal: true

require 'house_test/house_server'
require 'rack'
require 'rack/test'

RSpec.describe 'backend server' do
  let(:browser) { Rack::Test::Session.new(Rack::MockSession.new(HouseServer)) }
  it 'create new house with default data' do
    browser.post '/houses'
    expect(browser.last_response).to be_ok
    id = browser.last_response.body
    expect(HouseTest.get(id)).not_to be nil
  end
  it 'create new house with custom data' do
    browser.post '/houses', JSON.generate(name: 'test_house')
    expect(browser.last_response).to be_ok
    id = browser.last_response.body
    expect(HouseTest.get(id)).not_to be nil
  end
  it 'retrieve houses' do
    id = HouseTest.add
    browser.get '/houses'
    expect(browser.last_response).to be_ok
    houses = browser.last_response.body
    expect(houses.to_s).to include id
  end
  it 'update a house' do
    id = HouseTest.add name: 'Starting name'
    browser.patch("/houses/#{id}", JSON.generate(name: 'Second name'))
    expect(browser.last_response).to be_ok
    expect(browser.last_response.body).to include id
    expect(HouseTest.get(id).name).to eq 'Second name'
  end
  context 'specific house' do
    it 'not found' do
      browser.get '/houses/not_found_id'
      expect(browser.last_response.status).to eq 404
    end
    it 'found' do
      id = HouseTest.add
      browser.get "/houses/#{id}"
      expect(browser.last_response).to be_ok
      house = browser.last_response.body
      expect(house.to_s).to include id
    end
  end
end
