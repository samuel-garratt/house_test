# frozen_string_literal: true

RSpec.describe House do
  before(:all) do
    @pid = Process.spawn('ruby', 'exe/house_test', err: %w[logs/test_server.log w])
    puts "Starting test server at #{@pid}"
    sleep 3
  end
  it 'can create a new house' do
    house = House.new
    exchange = HouseHandler.get(house.id)
    expect(exchange.status_code).to eq 200
  end
  it 'retrieve name of a house' do
    house = House.new(name: 'test_house')
    expect(house.name).to eq 'test_house'
  end
  it 'update house' do
    house = House.new(name: 'first name')
    exchange = HouseHandler.get(house.id)
    expect(exchange.status_code).to eq 200
    house.name = '2nd name'
    expect(house.name).to eq '2nd name'
  end
  after(:all) do
    Process.kill(:QUIT, @pid)
  end
end
